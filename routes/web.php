<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GegekController;
//use App\Http\Controllers\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');

});

Route::get('/datasantri',[GegekController::class, 'index'])->name('datasantri');

Route::get('/tambahsantri',[GegekController::class, 'tambahsantri'])->name('tambahsantri');
Route::post('/insertdata',[GegekController::class, 'insertdata'])->name('insertdata');

Route::get('/tampilkandata/{id}',[GegekController::class,'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[GegekController::class,'updatedata'])->name('updatedata');

Route::get('/delete/{id}',[GegekController::class,'delete'])->name('delete');




