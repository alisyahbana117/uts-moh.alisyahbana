<?php

namespace App\Http\Controllers;
use App\models\Gegek;
use Illuminate\Http\Request;


class GegekController extends Controller
{
    public function index(){
        $data = Gegek::all();
        return view('datasantri',compact('data'));
    }

    public function tambahsantri(){
        return view('tambahdata');
    }

    public function insertdata(request $request){
        //dd($request->all());
        Gegek::create($request->all());
        return redirect()->route('datasantri');
    }

    public function tampilkandata($id){

        $data = Gegek::find($id);
        //dd($data);
        return view('tampildata', compact('data'));

    }

    public function updatedata(request $request, $id){
        $data = Gegek::find($id);
        $data->update($request->all());

        return redirect()->route('datasantri');
    }

    public function delete($id){
        $data = Gegek::find($id);
        $data->delete();
        return redirect()->route('datasantri');
    }
}
