<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GegekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gegeks')->insert([
            'nama'=>'moh ali syahbana',
            'alamat'=>'batukalangan',
            'tetala'=>'21 oktober 2002',
            'jeniskelamin'=>'pria',
            'nohtelpon'=>'083847845031',
            'email'=>'bana123@gmail.com',
        ]);
    }
}
