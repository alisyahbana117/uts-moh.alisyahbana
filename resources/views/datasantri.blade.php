@extends('layout.adminserver')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Santri Baru</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <div class="container">
        <a href="/tambahsantri"  class="btn btn-success">tambah +</a>
        <div class="row">

            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">nama</th>
                    <th scope="col">alamat</th>
                    <th scope="col">tetala</th>
                    <th scope="col">jenis kelamin</th>
                    <th scope="col">nohtelpon</th>
                    <th scope="col">email</th>
                    <th scope="col">di buat</th>
                    <th scope="col">aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1
                    @endphp
                    @foreach ($data as $row)
                    <tr>
                        <th scope="row">{{ $no++ }}</th>
                        <td>{{$row->nama}}</td>
                        <td>{{$row->alamat}}</td>
                        <td>{{$row->tetala}}</td>
                        <td>{{$row->jeniskelamin}}</td>
                        <td>0{{$row->nohtelpon}}</td>
                        <td>{{$row->email}}</td>
                        <td>{{$row->created_at->format('D M Y') }}</td>
                        <td>
                            <a href="/tampilkandata/{{ $row->id }}" class="btn btn-info">Edit</a>
                            <a href="/delete/{{ $row->id }}" class="btn btn-danger">Delete</a>
                        </td>
                      </tr>
                    @endforeach

                </tbody>
              </table>

        </div>
    </div>

</div>












@endsection
